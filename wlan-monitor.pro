# The name of your app.
# NOTICE: name defined in TARGET has a corresponding QML filename.
#         If name defined in TARGET is changed, following needs to be
#         done to match new name:
#         - corresponding QML filename must be changed
#         - desktop icon filename must be changed
#         - desktop filename must be changed
#         - icon definition filename in desktop file must be changed
TARGET = wlan-monitor

CONFIG += sailfishapp

SOURCES += src/wlan-monitor.cpp

OTHER_FILES += qml/wlan-monitor.qml \
    qml/cover/CoverPage.qml \
    qml/pages/FirstPage.qml \
    qml/pages/SecondPage.qml \
    rpm/wlan-monitor.spec \
    rpm/wlan-monitor.yaml \
    wlan-monitor.desktop

RESOURCES += \
    wlan-monitor.qrc

